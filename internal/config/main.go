package config

import "gitlab.com/rendyananta/vip-management-system/pkg/staff"

var Dsn = "file:vip-management-system.sqlite?_journal_mode=wal"

// Seeder for staff accounts
var staffList []staff.Staff