# Nodeflux Software Engineer Internship Assesment

[Nodeflux x Kampus Merdeka] Software Engineer Internship Assessment

# Overview
## VIP Management System

This application uses Golang and SQLite. 
The reasons behind the usage of golang, is because Golang is the new trend
in the backend engineering because of its simplicity and performance. 
**However, me as a backend engineer of VIP Management System try to learn to use golang.**
To store the data of the VIPs, this project uses SQLite. Since it's not a huge app and high load, so it just uses small amount of resource.
The application speed are depends on the disk used. SQLite also support for concurrency access because of write ahead logging feature.

Main library that used to built application is Gin Gonic for HTTP Router. 

## How to run application

Prebuilt application binary in the `bin` folder. Compilation machine uses Arch Linux (kernel version 5.12), with x86_64 CPU instruction set architecture.
If you are using another CPU with different instruction set like ARM machines (e.g. Apple M1 Chip), you should rebuild the binary.

### Run migrations
The migrations needed for application to run, because it's the foundation of the app. Open terminal in the directory and run.

```./bin/migration```

or using this command to run application without building it again.
```go run cmd/migration/main.go```

### Run applications
After running migrations, you can run 
```./bin/app```

```go run main.go```

### Note
> This project is unfinished, due to the first time learning golang for creating web service. 
> Me as an engineer shocked that this language having much different behaviour than any C-based languages (PHP, Java/Kotlin, Javascript) I usually worked on.
> So this is totally my miscalculation when choosing the tech stack before implementation.


### Known Bugs
* Basic Auth isn't implemented yet
* Attributes always returning null
* Update and query isn't tested yet
* UpdateArrivalStatus query is also isn't tested yet
