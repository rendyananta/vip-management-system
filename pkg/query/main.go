package query

import (
	"database/sql"
	"fmt"
	"gitlab.com/rendyananta/vip-management-system/internal/config"

	_ "github.com/mattn/go-sqlite3"
)

func InitDB() *sql.DB {
	db, err := sql.Open("sqlite3", config.Dsn)

	if err != nil {
		fmt.Println("Failed to initialized database")
		panic(err)
	}

	return db
}
