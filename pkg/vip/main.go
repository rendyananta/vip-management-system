package vip

import (
	"github.com/google/uuid"
	builder "gitlab.com/rendyananta/vip-management-system/pkg/query"
	"time"
)

type Vip struct {
	Id string				`json:"id" uri:"vip_id"`
	Name string				`json:"name"`
	CountryOfOrigin string	`json:"country_of_origin"`
	Eta time.Time			`json:"eta"`
	Photo string			`json:"photo"`
	Arrived bool			`json:"arrived"`
	Attributes []string		`json:"attributes"`
}

type Attribute struct {
	Id string				`json:"id"`
	Name string				`json:"name"`
	VipId string			`json:"vip_id"`
}

func Create(data Vip) (Vip, error) {
	// Generate UUID
	data.Id = uuid.NewString()

	db := builder.InitDB()

	createVip, err := db.Prepare(`
		insert into vips (id, name, country_of_origin, eta, photo) values (?, ?, ?, ?, ?)
	`)

	if err != nil {
		return Vip{}, err
	}

	_, err = createVip.Exec(data.Id, data.Name, data.CountryOfOrigin, data.Eta.String(), data.Photo)

	if err != nil {
		return Vip{}, err
	}

	_, err = CreateAttributes(data.Id, data.Attributes)

	if err != nil {
		return Vip{}, err
	}

	vip, err := Find(data.Id)

	if err != nil {
		return Vip{}, err
	}

	return vip, err
}

func Update(id string, data Vip) (Vip, error) {
	db := builder.InitDB()

	update, err := db.Prepare(`
		update vips set name = ?, country_of_origin = ?, eta = ?, photo = ? where id = ?
	`)

	if err != nil {
		return Vip{}, err
	}

	result, err := update.Exec(data.Name, data.CountryOfOrigin, data.Eta, data.Photo, id)
	if err != nil {
		return Vip{}, err
	}

	affectedRows, err := result.RowsAffected()

	if err != nil && affectedRows > 0 {
		return Vip{}, err
	}

	vip, err := Find(id)

	if err != nil {
		return Vip{}, err
	}

	return vip, err
}

func UpdateArrivalStatus(id string, status bool) (Vip, error) {
	db := builder.InitDB()

	update, err := db.Prepare(`update vips set arrived = ? where id = ?`)

	if err != nil {
		return Vip{}, err
	}

	result, err := update.Exec(status, id)
	if err != nil {
		return Vip{}, err
	}

	affectedRows, err := result.RowsAffected()

	if err != nil && affectedRows > 0 {
		return Vip{}, err
	}

	vip, err := Find(id)

	if err != nil {
		return Vip{}, err
	}

	return vip, err
}

func GetAll() ([]Vip, error) {
	db := builder.InitDB()

	var result []Vip

	getVip, err := db.Query("select * from vips")

	if err != nil {
		return []Vip{}, err
	}

	for getVip.Next() {
		var item Vip
		err := getVip.Scan(&item)

		if err != nil {
			return result, nil
		}

		result = append(result, item)
	}

	return result, nil
}

func Find(id string) (Vip, error)  {
	db := builder.InitDB()

	getVip := db.QueryRow("select * from vips where id = ?", id)

	vip := Vip{
		Id:              "",
		Name:            "",
		CountryOfOrigin: "",
		Eta:             time.Time{},
		Photo:           "",
		Arrived:         false,
		Attributes:      nil,
	}

	err := getVip.Scan(&vip.Id, &vip.Name, &vip.CountryOfOrigin, &vip.Eta, &vip.Photo, &vip.Arrived)

	if err != nil {
		return Vip{}, err
	}

	attributes, err := GetAttributes(vip.Id)

	if err != nil {
		vip.Attributes = attributes
	}

	return vip, nil
}

func CreateAttributes(id string, names []string) ([]string, error)  {
	db := builder.InitDB()

	if len(names) == 0 {
		return []string{}, nil
	}

	var parameters []interface{}
	query := "insert into vip_attributes (id, name, vip_id) values (?, ? , ?), "

	for i, attribute := range names {
		parameters = append(parameters, []interface{}{uuid.NewString(), attribute, id}...)
		if i == 0 {
			continue
		} else {
			if i == len(names) - 1 {
				query += `(?, ? , ?)`
			} else {
				query += `(?, ? , ?), `
			}
		}
	}


	createAttributes, err := db.Prepare(query)
	if err != nil {
		return names, err
	}

	_, err = createAttributes.Exec(parameters...)

	if err != nil {
		return names, err
	}

	return names, nil
}

func GetAttributes(id string) ([]string, error)  {
	db := builder.InitDB()

	getAttributes, err := db.Query("select name from vip_attributes where vip_id = ?", id)

	if err != nil {
		return nil, err
	}

	var attributes []string

	for getAttributes.Next() {
		var item string
		err := getAttributes.Scan(&item)

		if err != nil {
			return attributes, err
		}

		attributes = append(attributes, item)
	}

	return attributes, nil
}

// GetAttributesWhereIn
// this methods mimics modern orm to avoid n + 1 query problem
func GetAttributesWhereIn(ids []string) ([]Attribute, error)  {
	db := builder.InitDB()

	if len(ids) == 0 {
		return []Attribute{}, nil
	}

	var parameters []interface{}

	query := "select * from vip_attributes where vip_id in ?"

	for i, id := range ids {
		parameters = append(parameters, id)
		if i == 0 {
			continue
		} else {
			if i == len(ids) - 1 {
				query += `, ?`
			} else {
				query += `, ?, `
			}
		}
	}

	result, err := db.Query(query)

	if err != nil {
		return nil, err
	}

	var attributes []Attribute

	for result.Next() {
		var item Attribute
		err := result.Scan(&item)

		if err != nil {
			return attributes, err
		}

		attributes = append(attributes, item)
	}

	return attributes, nil
}