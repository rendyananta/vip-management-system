package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/rendyananta/vip-management-system/pkg/vip"
	"time"
)

type singleResponse struct {
	Ok      bool   		`json:"ok"`
	Message string 		`json:"message"`
	Data    interface{}	`json:"data"`
}

type listResponse struct {
	Ok      bool			`json:"ok"`
	Message string 			`json:"message"`
	Data    []interface{}	`json:"data"`
}

type actionResponse struct {
	Ok      bool   		`json:"ok"`
	Message string 		`json:"message"`
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	return r
}

func getAllVips(context *gin.Context)  {
	result, err := vip.GetAll()

	if err != nil {
		context.AbortWithStatusJSON(500, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	data := make([]interface{}, len(result))

	for i, v := range result {
		data[i] = v
	}

	context.JSON(200, listResponse{
		Ok:      true,
		Message: "",
		Data:    data,
	})
}

func createVip(context *gin.Context)  {
	// define request form validation
	type request struct {
		Name string				`json:"name" form:"user" binding:"required"`
		CountryOfOrigin string	`json:"country_of_origin" form:"country_of_origin" binding:"required"`
		Eta string				`json:"eta" form:"eta" binding:"required"`
		Photo string			`json:"photo" binding:"required"`
		Attributes []string		`json:"attributes" binding:"required"`
	}

	var req request

	err := context.ShouldBindJSON(&req)
	if err != nil {
		context.AbortWithStatusJSON(422, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	layoutFormat := "2006-01-02 15:04:05"
	eta, err := time.Parse(layoutFormat, req.Eta)

	if err != nil {
		context.AbortWithStatusJSON(422, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	create, err := vip.Create(vip.Vip{
		Name:            req.Name,
		CountryOfOrigin: req.CountryOfOrigin,
		Eta:             eta,
		Photo:           req.Photo,
		Attributes:      req.Attributes,
	})

	if err != nil {
		context.AbortWithStatusJSON(500, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	context.JSON(201, singleResponse{
		Ok:      true,
		Message: "VIP Successfully Created",
		Data:    create,
	})
}

func updateVip(context *gin.Context)  {
	var model vip.Vip

	if err := context.ShouldBindUri(&model); err != nil {
		context.AbortWithStatusJSON(422, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	// define request form validation
	type request struct {
		Name string				`json:"name" form:"user" binding:"required"`
		CountryOfOrigin string	`json:"country_of_origin" form:"country_of_origin" binding:"required"`
		Eta string				`json:"eta" form:"eta" binding:"required"`
		Photo string			`json:"photo" binding:"required"`
		Attributes []string		`json:"attributes" binding:"required"`
	}

	var req request

	err := context.ShouldBindJSON(&req)
	if err != nil {
		context.AbortWithStatusJSON(422, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	layoutFormat := "2006-01-02 15:04:05"
	eta, err := time.Parse(layoutFormat, req.Eta)

	if err != nil {
		context.AbortWithStatusJSON(422, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	item, err := vip.Update(model.Id, vip.Vip{
		Name:            req.Name,
		CountryOfOrigin: req.CountryOfOrigin,
		Eta:             eta,
		Photo:           req.Photo,
		Attributes:      req.Attributes,
	})

	if err != nil {
		context.AbortWithStatusJSON(500, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	context.JSON(200, singleResponse{
		Ok:      true,
		Message: "VIP successfully updated",
		Data:    item,
	})
}

func updateArrivalStatus(context *gin.Context)  {
	var model vip.Vip

	if err := context.ShouldBindUri(&model); err != nil {
		context.AbortWithStatusJSON(422, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	// define request form validation
	type request struct {
		Arrived bool			`json:"arrived"`
	}

	var req request

	err := context.ShouldBindJSON(&req)
	if err != nil {
		context.AbortWithStatusJSON(422, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	item, err := vip.UpdateArrivalStatus(model.Id, req.Arrived)

	if err != nil {
		context.AbortWithStatusJSON(500, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	context.JSON(200, singleResponse{
		Ok:      true,
		Message: "VIP successfully updated",
		Data:    item,
	})
}

func getVip(context *gin.Context)  {
	var model vip.Vip

	if err := context.ShouldBindUri(&model); err != nil {
		context.AbortWithStatusJSON(422, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	item, err := vip.Find(model.Id)

	if err != nil {
		context.AbortWithStatusJSON(500, actionResponse{
			Ok:      false,
			Message: err.Error(),
		})
		return
	}

	context.JSON(200, singleResponse{
		Ok:      true,
		Message: "Success",
		Data:    item,
	})
}

func defineVipRoutes(engine *gin.Engine) {
	group := engine.Group("/api/vips/")

	group.GET("/", getAllVips)
	group.POST("/", createVip)
	group.GET("/:vip_id", getVip)
	group.PUT("/:vip_id", updateVip)
	group.PATCH("/:vip_id/arrived", updateArrivalStatus)
}

func main()  {
	g := setupRouter()

	defineVipRoutes(g)
	err := g.Run()

	if err != nil {
		panic(err)
	}
}