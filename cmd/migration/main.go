package main

import (
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	builder "gitlab.com/rendyananta/vip-management-system/pkg/query"
)

func main()  {
	fmt.Println("Migrating database files...")
	CreateVipsTable()
	CreateVipAttributesTable()
	CreateStaffsTable()
}

func CreateVipsTable()  {
	db := builder.InitDB()

	query := `create table if not exists vips (
    	id char(16) primary key, 
    	name varchar(255) not null, 
    	country_of_origin varchar(30) not null, 
    	eta datetime not null, 
    	photo text nullable, 
    	arrived boolean default false
    )`

	_, err := db.Exec(query)

	if err != nil {
		fmt.Println("Failed executing query")
		panic(err)
	}

	fmt.Println("Table vips successfully created.")
}

func CreateVipAttributesTable() {
	db := builder.InitDB()

	query := `create table if not exists vip_attributes (
    	id char(16) primary key, 
    	name varchar(255) not null, 
    	vip_id char(16) not null, 
    	foreign key (vip_id) references vips (id)
    )`

	_, err := db.Exec(query)

	if err != nil {
		fmt.Println("Failed executing query")
		panic(err)
	}

	fmt.Println("Table vip_attributes successfully created.")
}

func CreateStaffsTable() {
	db := builder.InitDB()

	query := `create table if not exists staffs (
    	id char(16) primary key,
    	name varchar(255) not null,
    	username varchar(255) not null,
    	password varchar(255) not null
    )`

	_, err := db.Exec(query)

	if err != nil {
		fmt.Println("Failed executing query")
		panic(err)
	}

	fmt.Println("Table staffs successfully created.")
}
